#ifdef GL_ES
  precision mediump float;
  precision mediump int;
#endif

uniform vec2 resolution;
uniform float time;
uniform float fft_high;
uniform float fft_low;
uniform float beatValue;

void main() {
  vec2 position = ( gl_FragCoord.xy / resolution.xy );

  // convert frequency to colors
  vec3 col = vec3( fft_high, 4.0*fft_high*(1.0-fft_high), 2.0*(4.0-fft_high) );
  float color = 0.0;
  color += sin( position.x * cos( time / 30.0 ) * 80.0) + cos( position.y * cos( time / 30.0 ) * 80.0);
  color += cos( position.x * cos( time / 30.0 ) * 60.0) + cos( position.y * sin( time / 30.0 ) * 60.0);
  col += color;

  gl_FragColor = vec4(col, 1.0);
}
